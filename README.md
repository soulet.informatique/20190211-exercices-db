Exercices db du 2019-02-11
==========================

Simplon Chambéry Promo1

# 1.

Créer une base database.db avec sqlite3 à partir du fichier database.sql

# 2.

Installer DBeaver pour visualiser la base créée

# 3.

Ajouter un élément dans chaque table depuis DBeaver

# 4.

Combien y a-t-il d'éléments dans chaque table (post, user, product, message)?

# 5.

Lister les éléments de la table user en ligne de commande

# 6.

Refaire les exercices 3 et 4 depuis sqlite3 en ligne de commande

# 7.

Modifier la base de données pour qu'elle corresponde à votre site entreprise

# 8. Sqlite et PHP

## 8.1

Lister les appels à réaliser en PHP pour interroger la base

## 8.2

Quelle extension PHP doit-on installer ?

## 8.3

Réaliser un fichier PHP qui liste la table user et donne le résultat dans une page HTML basique

## 8.4 PDO ?